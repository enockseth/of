var wms_layers = [];


        var lyr_OpenStreetMap_0 = new ol.layer.Tile({
            'title': 'OpenStreetMap',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: ' ',
                url: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'
            })
        });

        var lyr_GoogleSatellite_1 = new ol.layer.Tile({
            'title': 'Google Satellite',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: ' &middot; <a href="https://www.google.at/permissions/geoguidelines/attr-guide.html">Map data ©2015 Google</a>',
                url: 'https://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}'
            })
        });

        var lyr_MaxarAccraMosaic2019_2 = new ol.layer.Tile({
            'title': 'Maxar Accra Mosaic 2019',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: ' ',
                url: 'https://tiles.openaerialmap.org/5ea05b54411bed0005680396/0/5ea05b54411bed0005680397/{z}/{x}/{y}'
            })
        });

        var lyr_UAVMay2020_3 = new ol.layer.Tile({
            'title': 'UAV May 2020',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: ' ',
                url: 'https://tiles.openaerialmap.org/5ed24f7157676e00057f5843/0/5ed24f7157676e00057f5844/{z}/{x}/{y}'
            })
        });
var format_AOI_4 = new ol.format.GeoJSON();
var features_AOI_4 = format_AOI_4.readFeatures(json_AOI_4, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_AOI_4 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_AOI_4.addFeatures(features_AOI_4);
var lyr_AOI_4 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_AOI_4, 
                style: style_AOI_4,
                interactive: true,
                title: '<img src="styles/legend/AOI_4.png" /> AOI'
            });
var format_AOI_II_5 = new ol.format.GeoJSON();
var features_AOI_II_5 = format_AOI_II_5.readFeatures(json_AOI_II_5, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_AOI_II_5 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_AOI_II_5.addFeatures(features_AOI_II_5);
var lyr_AOI_II_5 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_AOI_II_5, 
                style: style_AOI_II_5,
                interactive: true,
                title: '<img src="styles/legend/AOI_II_5.png" /> AOI_II'
            });

lyr_OpenStreetMap_0.setVisible(true);lyr_GoogleSatellite_1.setVisible(true);lyr_MaxarAccraMosaic2019_2.setVisible(true);lyr_UAVMay2020_3.setVisible(true);lyr_AOI_4.setVisible(true);lyr_AOI_II_5.setVisible(true);
var layersList = [lyr_OpenStreetMap_0,lyr_GoogleSatellite_1,lyr_MaxarAccraMosaic2019_2,lyr_UAVMay2020_3,lyr_AOI_4,lyr_AOI_II_5];
lyr_AOI_4.set('fieldAliases', {'Name': 'Name', 'Description': 'Description', });
lyr_AOI_II_5.set('fieldAliases', {'landuse': 'landuse', });
lyr_AOI_4.set('fieldImages', {'Name': '', 'Description': '', });
lyr_AOI_II_5.set('fieldImages', {'landuse': '', });
lyr_AOI_4.set('fieldLabels', {'Name': 'no label', 'Description': 'no label', });
lyr_AOI_II_5.set('fieldLabels', {'landuse': 'no label', });
lyr_AOI_II_5.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});